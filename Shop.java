import java.util.Scanner;
public class Shop {

	//MAIN METHOD
	public static void main(String[] args) {
		//INITIALIZING THE SCANNER AND SONG OBJECTS
		Scanner reader = new Scanner(System.in);
		Song[] songs = new Song[4];
		
		//FOR LOOP FOR FILLING ARRAY FIELDS
		for( int i = 0; i <= songs.length - 1; i++) {
			songs[i] = new Song();
			
			//ASKING FOR FIELDS AND FILLING THEM OUT
			System.out.println("Insert song title:");
			songs[i].title = reader.nextLine();
			
			System.out.println("Insert the artist of the song:");
			songs[i].artist = reader.nextLine();
			
			System.out.println("Insert the number of plays here");
			songs[i].plays = Integer.parseInt(reader.nextLine());
		}
		
		//PRINTING LAST SONGS FIELDS
		System.out.println("Title of last song is: " + songs[songs.length - 1].title);
		System.out.println("Artist of last song is: " + songs[songs.length - 1].artist);
		System.out.println("The number of plays of the last song is: " + songs[songs.length - 1].plays);
	
		//CALLING THE INSTANCE METHOD ON THE LAST SONG
		songs[songs.length-1].reaction();
	}
}