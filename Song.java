public class Song {
	
	//INITIALIZING MY FIELDS
	public String artist;
	public String title;
	public int plays;
	
	//CREATING MY INSTANCE METHOD 
	public void reaction() {
		//IF THE ARTIST IS BTS IT PRINTS "Wow!"
		//OTHERWISE IT PRINTS "Cool!"
		if (this.artist.equals("bts")) {
			System.out.println("Wow!");
		} else {
			System.out.println("Cool!");
		}
	}
}